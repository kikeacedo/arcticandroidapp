# ARCTIC #
* Arctic es una aplicación de Android que gracias a un sensor acoplado al esquí, permite al usuario ver estadísticas muy precisas de cómo esquía.
* Version 1.0
* [Uso de IMUduino](https://femto.io/products/imuduino)

### Actualizaciones ###

* Bateria LiPo con conector Micro-USB para alimentar a IMUduino

![IMG_0845.jpg](https://bitbucket.org/repo/y49EM9/images/2849568167-IMG_0845.jpg)
![IMG_0844.jpg](https://bitbucket.org/repo/y49EM9/images/1795456051-IMG_0844.jpg)

* Pantalla de actividad con bloqueo

![device-2016-06-29-211003.png](https://bitbucket.org/repo/y49EM9/images/2269985324-device-2016-06-29-211003.png)

* Lista de actividades

![device-2016-06-29-211025.png](https://bitbucket.org/repo/y49EM9/images/713449983-device-2016-06-29-211025.png)

* Pantalla de ajustes con opción de olvidar dispositivo BLE o de eliminar cuenta de usuario

![device-2016-06-29-211037.png](https://bitbucket.org/repo/y49EM9/images/2847707679-device-2016-06-29-211037.png)


### Trabajo por realizar ###

* Creación de test
* Creación de una carcasa que se acople al esqui para el sensor

### Contacto ###

* Kike Acedo
* kike.acedo@gmail.com